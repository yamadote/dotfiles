-- Set <space> as the leader key
-- See `:help mapleader`
vim.g.mapleader = " "

require("custom.options")
require("custom.keymaps")
require("custom.autocmd")

-- [[ Install `lazy.nvim` plugin manager ]]
-- See `:help lazy.nvim.txt`
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.uv.fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
	if vim.v.shell_error ~= 0 then
		error("Error cloning lazy.nvim:\n" .. out)
	end
end
vim.opt.rtp:prepend(lazypath)

-- [[ Configure and install plugins ]]
require("lazy").setup({
	spec = { { import = "plugins" } },
	install = { colorscheme = { "onedark" } },
	change_detection = { enabled = true, notify = false },
	ui = {
		border = vim.g.border,
		backdrop = 100,
		icons = {
			task = vim.g.done_icon,
			ft = "$ ",
			init = "# ",
			loaded = "*",
			not_loaded = "*",
			source = "&",
		},
	},
})
