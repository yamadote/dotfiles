vim.opt_local.expandtab = true
vim.opt_local.tabstop = 4
vim.opt_local.shiftwidth = 0
vim.opt_local.commentstring = "// %s"

vim.keymap.set("i", "-", function()
	local char = require("custom.utils").get_current_subline(0, 0)
	if char:match("%S") then
		return "->"
	end
	return "-"
end, { buffer = 0, expr = true })

vim.keymap.set("i", ">", function()
	local char = require("custom.utils").get_current_subline(-1, 0)
	if char == "->" then
		return ""
	end
	return ">"
end, { buffer = 0, expr = true })
