vim.opt_local.spell = true
vim.opt_local.list = false

-- autoclose commit window on save
vim.keymap.set("n", "<C-s>", "<cmd>wq<cr>", { buffer = 0 })
