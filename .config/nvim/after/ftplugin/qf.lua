vim.keymap.set("n", "J", "<cmd>lnext<bar>wincmd p<cr>", { buffer = 0 })
vim.keymap.set("n", "K", "<cmd>lprev<bar>wincmd p<cr>", { buffer = 0 })
vim.keymap.set("n", "p", "<CR><C-w>p", { buffer = 0 })

-- Change window highgt
local height = math.floor(vim.o.lines * 0.3)
vim.cmd("wincmd " .. height .. "_")

-- Refresh statusline
vim.wo.statusline = "%!v:lua.StatusLine.active()"
