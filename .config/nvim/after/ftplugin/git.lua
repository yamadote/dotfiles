vim.keymap.set("n", "<Up>", "(", { buffer = 0, remap = true })
vim.keymap.set("n", "<Down>", ")", { buffer = 0, remap = true })

vim.keymap.set("n", "J", ")p", { buffer = 0, remap = true })
vim.keymap.set("n", "K", "(p", { buffer = 0, remap = true })
