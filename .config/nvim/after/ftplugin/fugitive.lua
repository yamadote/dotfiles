vim.keymap.set("n", "<Up>", "(", { buffer = 0, remap = true })
vim.keymap.set("n", "<Down>", ")", { buffer = 0, remap = true })

vim.keymap.set("n", "<Tab>", "-", { buffer = 0, remap = true })
vim.keymap.set("n", "<BS>", "<", { buffer = 0, remap = true })
vim.keymap.set("n", "p", "<CR><C-w>p", { buffer = 0, remap = true })
