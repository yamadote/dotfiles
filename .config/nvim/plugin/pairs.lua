local function autopair(first, second)
	return function()
		return first .. second .. "<Left>"
	end
end

local function skip(input)
	return function()
		local next = require("custom.utils").get_current_subline(1, 1)
		if next == input then
			return input .. "<Del>"
		end
		return input
	end
end

vim.api.nvim_create_autocmd("FileType", {
	group = vim.api.nvim_create_augroup("custom_pairs_setup", { clear = true }),
	callback = function(event)
		if vim.bo[event.buf].filetype == "TelescopePrompt" then
			return
		end

		local function imap(lhs, rhs)
			vim.keymap.set("i", lhs, rhs, { buffer = event.buf, expr = true })
		end

		imap("{", autopair("{", "}"))
		imap("(", autopair("(", ")"))
		imap("[", autopair("[", "]"))

		imap("}", skip("}"))
		imap(")", skip(")"))
		imap("]", skip("]"))

		-- Expend pair on enter
		imap("<CR>", function()
			local pair = require("custom.utils").get_current_subline(0, 1)
			local pattern = { "{}", "()", "[]" }
			if vim.tbl_contains(pattern, pair) then
				return "<CR><Up><End><CR>"
			end
			return "<CR>"
		end)

		-- Remove pair on backspace
		imap("<BS>", function()
			local pair = require("custom.utils").get_current_subline(0, 1)
			local pattern = { "{}", "()", "[]" }
			if vim.tbl_contains(pattern, pair) then
				return "<Del><BS>"
			end
			return "<BS>"
		end)
	end,
})
