-- See `:help statusline`
-- todo: add gitsigns

local function filepath()
	local winid = vim.g.statusline_winid
	local buf = vim.api.nvim_win_get_buf(winid)
	local path = require("custom.utils").get_buffer_name(buf)
	if path == "" then
		return " [No Name] "
	end
	local relative = vim.fn.fnamemodify(path, ":.")
	if relative ~= "" then
		path = relative
	end
	return string.format(" %%<%s ", path)
end

local function modified()
	local winid = vim.g.statusline_winid
	local buf = vim.api.nvim_win_get_buf(winid)
	if not vim.api.nvim_get_option_value("modifiable", { buf = buf }) then
		return "[-] "
	end
	if vim.api.nvim_get_option_value("modified", { buf = buf }) then
		return "[+] "
	end
	return ""
end

local function lsp()
	local result = ""
	local buf = vim.api.nvim_win_get_buf(vim.g.statusline_winid)

	local errors = vim.diagnostic.get(buf, { severity = vim.diagnostic.severity.ERROR })
	if #errors ~= 0 then
		result = result .. " %#LspDiagnosticsSignError#E:" .. #errors
	end

	local warnings = vim.diagnostic.get(buf, { severity = vim.diagnostic.severity.WARN })
	if #warnings ~= 0 then
		result = result .. " %#LspDiagnosticsSignWarn#W:" .. #warnings
	end

	local hints = vim.diagnostic.get(buf, { severity = vim.diagnostic.severity.HINT })
	if #hints ~= 0 then
		result = result .. " %#LspDiagnosticsSignHint#H:" .. #hints
	end

	local info = vim.diagnostic.get(buf, { severity = vim.diagnostic.severity.INFO })
	if #info ~= 0 then
		result = result .. " %#LspDiagnosticsSignInfo#I:" .. #info
	end

	if result ~= "" then
		return result .. " "
	end
	return result
end

local function preview()
	local winid = vim.g.statusline_winid
	if vim.api.nvim_get_option_value("previewwindow", { win = winid }) then
		return " preview "
	end
	return ""
end

local function wrap()
	local winid = vim.g.statusline_winid
	if vim.api.nvim_get_option_value("wrap", { win = winid }) then
		return " wrap "
	end
	return ""
end

local function spell()
	local winid = vim.g.statusline_winid
	if vim.api.nvim_get_option_value("spell", { win = winid }) then
		return " spell "
	end
	return ""
end

local function tabs()
	local winid = vim.g.statusline_winid
	local buf = vim.api.nvim_win_get_buf(winid)
	if not vim.api.nvim_get_option_value("modifiable", { buf = buf }) then
		return ""
	end
	if vim.api.nvim_get_option_value("expandtab", { buf = buf }) then
		local shiftwidth = require("custom.utils").shiftwidth(buf)
		return string.format(" %s spaces ", shiftwidth)
	end
	return " tabs "
end

local function encoding()
	local winid = vim.g.statusline_winid
	local buf = vim.api.nvim_win_get_buf(winid)
	local value = vim.api.nvim_get_option_value("fileencoding", { buf = buf })
	if value and value ~= "" then
		return string.format(" %s ", value)
	end
	return ""
end

local function filetype()
	local winid = vim.g.statusline_winid
	local buf = vim.api.nvim_win_get_buf(winid)
	local value = vim.api.nvim_get_option_value("filetype", { buf = buf })
	if value and value ~= "" then
		return string.format(" %s ", value)
	end
	return ""
end

local function extra()
	return table.concat({
		"%=%#StatusLineExtra#",
		preview(),
		tabs(),
		spell(),
		wrap(),
		encoding(),
		filetype(),
		" %P ",
	})
end

StatusLine = {}

function StatusLine.active()
	return table.concat({
		filepath(),
		modified(),
		lsp(),
		extra(),
	})
end

function StatusLine.inactive()
	return table.concat({
		filepath(),
		modified(),
		extra(),
	})
end

local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

autocmd({ "WinEnter", "BufEnter", "DiagnosticChanged" }, {
	group = augroup("statusline_active", { clear = true }),
	callback = function()
		vim.wo.statusline = "%!v:lua.StatusLine.active()"
	end,
})

autocmd({ "WinLeave", "BufLeave" }, {
	group = augroup("statusline_inctive", { clear = true }),
	callback = function()
		vim.wo.statusline = "%!v:lua.StatusLine.inactive()"
	end,
})
