-- Disable color column highlight
vim.api.nvim_set_hl(0, "ColorColumn", {})

local namespace = vim.api.nvim_create_namespace("virtcolumn")
vim.api.nvim_set_decoration_provider(namespace, {
	on_win = function(_, win, bufnr, topline, botline_guess)
		pcall(vim.api.nvim_buf_clear_namespace, bufnr, namespace, 0, -1)

		if vim.api.nvim_get_option_value("diff", { win = win }) then
			return false
		end

		if vim.api.nvim_get_option_value("list", { win = win }) then
			return false
		end

		if not vim.api.nvim_get_option_value("modifiable", { buf = bufnr }) then
			return false
		end

		local filetype = vim.api.nvim_get_option_value("filetype", { buf = bufnr })
		if filetype == "help" then
			return false
		end

		local utils = require("custom.utils")
		local colorcolumn = vim.api.nvim_get_option_value("colorcolumn", { win = win })
		local textwidth = vim.api.nvim_get_option_value("textwidth", { buf = bufnr })
		local expandtab = vim.api.nvim_get_option_value("expandtab", { buf = bufnr })
		local shiftwidth = utils.shiftwidth(bufnr)

		if utils.is_floating_win(win) then
			return false
		end

		---@type number[]
		local columns = {}
		for _, c in ipairs(vim.split(colorcolumn, ",")) do
			if vim.startswith(c, "+") then
				if textwidth ~= 0 then
					table.insert(columns, textwidth + tonumber(c:sub(2)))
				end
			elseif vim.startswith(c, "-") then
				if textwidth ~= 0 then
					table.insert(columns, textwidth - tonumber(c:sub(2)))
				end
			elseif tonumber(c) then
				table.insert(columns, tonumber(c))
			end
		end

		local leftcol = vim.api.nvim_win_call(win, function()
			return vim.fn.winsaveview().leftcol
		end)

		local function extmark(line, col, text)
			local virt_text_win_col = col - leftcol
			if virt_text_win_col >= 0 then
				pcall(vim.api.nvim_buf_set_extmark, bufnr, namespace, line, 0, {
					virt_text = { text },
					virt_text_pos = "overlay",
					hl_mode = "combine",
					virt_text_win_col = virt_text_win_col,
					priority = 1,
				})
			end
		end

		local i = topline
		while i <= botline_guess do
			local line = utils.get_line(bufnr, i + 1)

			local indent = 0
			if line then
				if expandtab then
					indent = math.ceil(#line:match("^ *") / shiftwidth)
				else
					indent = #line:match("^\t*")
				end
			end

			if indent > 0 then
				for j = 0, indent - 1 do
					extmark(i, j * shiftwidth, { "▏", "WinSeparator" })
				end
			end

			local width = vim.api.nvim_win_call(win, function()
				return vim.fn.virtcol({ i + 1, "$" }) - 1
			end)

			for _, column in ipairs(columns) do
				if width < column then
					extmark(i, column, { "▏", "WinSeparator" })
				end
			end

			local fold_end = vim.api.nvim_win_call(win, function()
				return vim.fn.foldclosedend(i)
			end)

			-- checks if line is folded
			if fold_end ~= -1 then
				i = fold_end
			end

			i = i + 1
		end
	end,
})
