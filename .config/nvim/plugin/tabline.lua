-- See `:help tabline`
local function tabline()
	local s = ""
	for index = 1, vim.fn.tabpagenr("$") do
		local winnr = vim.fn.tabpagewinnr(index)
		local buflist = vim.fn.tabpagebuflist(index)
		local bufname = vim.fn.bufname(buflist[winnr])

		s = s .. "%" .. index .. "T"
		if index == vim.fn.tabpagenr() then
			s = s .. "%#TabLineSel#"
		else
			s = s .. "%#TabLine#"
		end

		local title = vim.fn.fnamemodify(bufname, ":t")
		if title == "" then
			title = "No Name"
		end
		if bufname:find("^oil://") then
			title = "No Name"
		end
		s = s .. "  " .. title .. " "
	end

	s = s .. "%#TabLineFill#"
	return s
end

function _G.nvim_tabline()
	return tabline()
end
vim.o.tabline = "%!v:lua.nvim_tabline()"
