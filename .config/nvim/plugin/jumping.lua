-- Jump forward to next line reference
vim.keymap.set("n", "<C-f>", function()
	local patterns = { "%." }
	if vim.bo.filetype == "php" then
		patterns = { "::", "->" }
	end
	if vim.bo.filetype == "lua" then
		table.insert(patterns, ":")
	end

	local col = vim.fn.col(".")
	local line = vim.api.nvim_get_current_line()
	local subline = string.sub(line, col)

	local matches = {}
	for _, pattern in pairs(patterns) do
		local _, match = string.find(subline, pattern)
		if match then
			table.insert(matches, match)
		end
	end

	if vim.tbl_isempty(matches) then
		return
	end

	local shift = math.min(unpack(matches))
	local row = vim.fn.line(".")
	vim.api.nvim_win_set_cursor(0, { row, col + shift - 1 })
end)

-- Jump to line number
vim.keymap.set("n", "S", function()
	local topline = vim.fn.line("w0")
	local botline = vim.fn.line("w$")

	local i = topline
	while i <= botline do
		if math.fmod(i, 100) == vim.v.count then
			vim.cmd("normal! " .. i .. "G")
			return
		end
		i = i + 1
	end
end)
