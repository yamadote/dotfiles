vim.ui.select = function(items, opts, on_choice)
	local actions = require("telescope.actions")
	local state = require("telescope.actions.state")
	local pickers = require("telescope.pickers")
	local finders = require("telescope.finders")
	local conf = require("telescope.config").values

	local results = {}
	for idx, item in ipairs(items) do
		table.insert(results, {
			idx = idx,
			item = item,
		})
	end

	local entry_maker = function(value)
		local text = opts.format_item(value.item)
		if opts.kind == "codeaction" then
			text = value.idx .. ": " .. text
		end
		return {
			display = text,
			ordinal = text,
			value = value,
		}
	end

	-- schedule_wrap because closing the windows is deferred
	-- See https://github.com/nvim-telescope/telescope.nvim/pull/2336
	-- And we only want to dispatch the callback when we're back in the original win
	on_choice = vim.schedule_wrap(on_choice)

	local defaults = {
		prompt_title = opts.prompt,
		previewer = false,
		finder = finders.new_table({
			results = results,
			entry_maker = entry_maker,
		}),
		sorter = conf.generic_sorter(opts),
		attach_mappings = function(prompt_bufnr)
			actions.select_default:replace(function()
				local selection = state.get_selected_entry()
				local callback = on_choice
				-- Replace on_choice with a no-op so closing doesn't trigger it
				on_choice = function(_, _) end
				actions.close(prompt_bufnr)
				if not selection then
					-- User did not select anything.
					callback(nil, nil)
					return
				end
				callback(selection.value.item, selection.value.idx)
			end)

			actions.close:enhance({
				post = function()
					on_choice(nil, nil)
				end,
			})

			return true
		end,
	}

	local picker_opts = {}
	pickers.new(picker_opts, defaults):find()
end
