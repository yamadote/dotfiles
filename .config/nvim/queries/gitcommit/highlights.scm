(comment) @comment
(generated_comment) @comment

(branch) @markup.link
(change) @keyword
(filepath) @string.special.path
(arrow) @punctuation.delimiter
(subject) @text @spell

(subject (subject_prefix) @function @nospell)
(prefix (type) @keyword @nospell)
(prefix (scope) @variable.parameter @nospell)
(prefix [ "(" ")" ":" ] @punctuation.delimiter)
(prefix "!" @punctuation.special)

(message) @text @spell
(trailer (token) @label)
(breaking_change (token) @comment.error)
(breaking_change (value) @none @spell)
(scissor) @comment
