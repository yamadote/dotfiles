(variable_name) @nospell
(tag_name) @attribute @nospell
(document) @annotation

(tag (tag_name) @_tag (#eq? @_tag "@param") (variable_name) @variable.parameter)
(tag (tag_name) @_tag (#eq? @_tag "@property") (variable_name) @variable.member)
(tag (tag_name) @_tag (#eq? @_tag "@var") (variable_name) @variable)
(tag (tag_name) @_tag (#eq? @_tag "@function.method") (name) @function.method)

(parameter (variable_name) @variable.parameter)
[(array_type) (primitive_type) (named_type) (optional_type)] @type @nospell
(tag [(author_name) (version) (email_address) (uri)] @none @nospell)
