-- See `:help vim.keymap.set()`
local map = vim.keymap.set
local fn = require("custom.utils").fn

map({ "n", "v" }, "<C-l>", function()
	vim.cmd("nohlsearch") -- Clear search highlight
	vim.cmd("diffupdate") -- Update the diff highlighting and folds
	vim.cmd("stopinsert") -- Clear message
end)

map("n", "<C-s>", "<cmd>write<cr>") -- Save document
map("n", "<C-t>", "<cmd>tabnew<cr>") -- Open new tab
map("n", "<C-x>", "<cmd>bdelete<cr>") -- Delete current buffer

map("n", "<leader>i", "<cmd>Inspect<cr>") -- Display treesitter items
map("n", "<leader>rl", "<cmd>edit!<cr>") -- Reload buffer
map("n", "<leader>rr", "<cmd>LspRestart<cr>") -- Restart LSP
map("n", "<leader><leader>", "<cmd>messages<cr>") -- Display messages
map("n", "<C-w>m", "<C-w>|<C-w>_") -- Maximize the window

-- Restore cursor position
local function restore(action)
	return "msHmt`s" .. action .. "`tzt`s"
end

-- Keep cursor while word searching
map("n", "*", restore("*"))
map("n", "g*", restore("g*"))
map("n", "#", restore("#"))
map("n", "g#", restore("g#"))

-- Keep cursor while line joining
map("n", "J", restore("J"))

-- Execute lua code
map("n", "<leader>x", ":%lua<cr>")
map("v", "<leader>x", ":lua<cr>")

-- Visually select all document
map({ "n", "x" }, "<C-a>", "<Esc>ggVG")

-- Terminal
map("n", "<leader>t", "<cmd>new|term<cr>")
map("n", "<F12>", "<cmd>new|term<cr>")
map("t", "<F12>", "<cmd>bd!<cr>")
map("t", "<C-d>", "<cmd>bd!<cr>")
map("t", "<C-\\>", "<C-\\><C-n>")

-- Better q key mapping
map("n", "q", "<cmd>tabclose<cr>")
map("n", "<C-q>", "q")

-- Increment/Decrement
map("n", "+", "<C-a>")
map("n", "-", "<C-x>")

-- Go up one directory
map("n", "gh", "<cmd>cd ..<cr>")

-- Go down one directory
map("n", "gl", function()
	local path = require("custom.utils").get_buffer_name(0)
	local relative = vim.fn.fnamemodify(path, ":.")
	if relative:find("/") then
		local directory = relative:match("[^/]*")
		vim.cmd(string.format("cd %s", directory))
	end
end)

-- Line navigation
map({ "n", "x", "o" }, "H", "^")
map({ "n", "x", "o" }, "L", "$")

-- Enable startofline line movement
map({ "n", "x" }, "j", "+")
map({ "n", "x" }, "k", "-")

-- Clipboard register
map({ "n", "x" }, "<leader>y", [["+y]])

-- Yank document path
map("n", "<leader>ydp", function()
	local path = require("custom.utils").get_buffer_name(0)
	vim.fn.setreg("+", vim.fn.fnamemodify(path, ":."))
	print('Document path yanked into "+')
end)

-- Yank document name
map("n", "<leader>ydn", function()
	local name = vim.fn.expand("%:t")
	if name:match("%S") then
		vim.fn.setreg("+", name)
		print('Document name yanked into "+')
	end
end)

-- Open diagnostics list
map("n", "<leader>dl", vim.diagnostic.setloclist, { desc = "Document diagnostics list" })

-- Diagnostic navigation
map("n", "[d", vim.diagnostic.goto_prev, { desc = "Previous diagnostic message" })
map("n", "]d", vim.diagnostic.goto_next, { desc = "Next diagnostic message" })

local error = { severity = vim.diagnostic.severity.ERROR }
map("n", "[e", fn(vim.diagnostic.goto_prev, error), { desc = "Previous error" })
map("n", "]e", fn(vim.diagnostic.goto_next, error), { desc = "Next error" })

-- Quickfix list navigation
map("n", "[q", "<cmd>cprev<cr>", { desc = "Previous quickfix entry" })
map("n", "]q", "<cmd>cnext<cr>", { desc = "Next quickfix entry" })

-- Location list navigation
map("n", "[l", "<cmd>lprev<cr>", { desc = "Previous location list entry" })
map("n", "]l", "<cmd>lnext<cr>", { desc = "Next location list entry" })

-- Buffer list navigation
map("n", "[b", "<cmd>bprev<cr>", { desc = "Previous buffer" })
map("n", "]b", "<cmd>bnext<cr>", { desc = "Next buffer" })

-- Window list navigation
map("n", "[w", "<C-w>W", { desc = "Previous window" })
map("n", "]w", "<C-w>w", { desc = "Next window" })

-- Tab list navigation
map("n", "[t", "gT", { desc = "Previous tab" })
map("n", "]t", "gt", { desc = "Next tab" })

-- Toggles
map("n", "<leader>ow", "<cmd>setlocal wrap!<cr>", { desc = "Toggle wrapping" })
map("n", "<leader>or", "<cmd>setlocal rnu!<cr>", { desc = "Toggle relative numbers" })
map("n", "<leader>on", "<cmd>setlocal nu!<cr>", { desc = "Toggle line numbers" })
map("n", "<leader>ol", "<cmd>setlocal list!<cr>", { desc = "Toggle list mode" })
map("n", "<leader>os", "<cmd>setlocal spell!<cr>", { desc = "Toggle spell checker" })

map("n", "<leader>o4", "<cmd>setlocal ts=4<cr>")
map("n", "<leader>o2", "<cmd>setlocal ts=2<cr>")

-- Toggle tab expand
map("n", "<leader>oe", function()
	vim.cmd("setlocal expandtab!")
	vim.wo.statusline = "%!v:lua.StatusLine.active()"
end, { desc = "Toggle tab expand" })

-- Toggle colorcolumn
map("n", "<leader>oc", function()
	if vim.wo.colorcolumn == "120" then
		vim.opt_local.colorcolumn = ""
	else
		vim.opt_local.colorcolumn = "120"
	end
end, { desc = "Toggle colorcolumn" })

-- Toggle foldcolumn
map("n", "<leader>of", function()
	if vim.wo.foldcolumn == "0" then
		vim.opt_local.foldcolumn = "2"
	else
		vim.opt_local.foldcolumn = "0"
	end
end, { desc = "Toggle foldcolumn" })
