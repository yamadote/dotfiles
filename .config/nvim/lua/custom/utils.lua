local M = {}

function M.is_floating_win(win)
	local config = vim.api.nvim_win_get_config(win)
	return config.relative ~= ""
end

function M.get_preview_win()
	for _, winid in ipairs(vim.api.nvim_tabpage_list_wins(0)) do
		if vim.api.nvim_win_is_valid(winid) and vim.wo[winid].previewwindow then
			return winid
		end
	end
end

function M.get_buffer_name(buf)
	local path = vim.api.nvim_buf_get_name(buf)
	return path:gsub("^oil://", "")
end

function M.get_current_subline(i, j)
	local position = vim.api.nvim_win_get_cursor(0)
	local line = vim.api.nvim_get_current_line()
	return string.sub(line, position[2] + i, position[2] + j)
end

function M.get_line(buf, row)
	return vim.api.nvim_buf_get_lines(buf, row - 1, row, false)[1]
end

function M.get_visual_range()
	local cursor = vim.api.nvim_win_get_cursor(0)
	local vpos = vim.fn.getpos("v")

	if cursor[1] < vpos[2] then
		return { cursor[1], vpos[2] }
	end
	return { vpos[2], cursor[1] }
end

function M.shiftwidth(buf)
	local shiftwidth = vim.api.nvim_get_option_value("shiftwidth", { buf = buf })
	if shiftwidth == 0 then
		return vim.api.nvim_get_option_value("tabstop", { buf = buf })
	end
	return shiftwidth
end

function M.leadmultispace(length)
	local listchars = vim.opt.listchars:get()
	local tab = vim.fn.str2list(listchars.tab)
	local space = vim.fn.str2list(listchars.multispace)
	local lead = { tab[1] }
	for i = 2, length do
		lead[i] = space[1]
	end
	return vim.fn.list2str(lead)
end

function M.fn(f, ...)
	local args = { ... }
	return function(...)
		return f(unpack(args), ...)
	end
end

return M
