local M = {}

function M.setup()
	local luasnip = require("luasnip")
	local s = luasnip.snippet
	local i = luasnip.insert_node
	local f = luasnip.function_node
	local fmt = require("luasnip.extras.fmt").fmt

	local function copy(args)
		return args[1][1]
	end

	local function var(args)
		local input = args[1][1]
		return "$" .. input
	end

	local function upper(args)
		local input = args[1][1]
		return string.gsub(input, "^%l", string.upper)
	end

	local function setter_snippet()
		local str = "/**\n * @param {} {}\n */\npublic function set{}({} {}): void\n{{\n\t$this->{} = {};\n}}"
		return fmt(str, { f(copy, 2), f(var, 1), f(upper, 1), i(2, "int"), f(var, 1), i(1, "property"), f(var, 1) })
	end

	local function getter_snippet()
		local str = "/**\n * @return {}\n */\npublic function get{}(): {}\n{{\n\treturn $this->{};\n}}"
		return fmt(str, { f(copy, 2), f(upper, 1), i(2, "int"), i(1, "property") })
	end

	local function foreach_snippet()
		local str = "foreach ({} as {}) {{\n\t{}\n}}"
		return fmt(str, { i(1, "$list"), i(2, "$item"), i(0) })
	end

	luasnip.add_snippets("php", {
		s("/**", fmt("/**\n * {}\n */", { i(0) })),
		s("pubf", fmt("public function {}", { i(0) })),
		s("prif", fmt("private function {}", { i(0) })),
		s("prof", fmt("protected function {}", { i(0) })),
		s("pubsf", fmt("public static function {}", { i(0) })),
		s("var", fmt("var_dump({});", { i(0) })),
		s("set", setter_snippet()),
		s("get", getter_snippet()),
		s("fore", foreach_snippet()),
	})
end

return M
