-- See `:help vim.opt`
vim.g.border = "single"
vim.g.done_icon = "󰸞"

-- Fix flickering
vim.opt.lazyredraw = true

-- Make line numbers default
vim.opt.number = true
vim.opt.numberwidth = 5

-- Disable ruler, use CTRL-G instead
vim.opt.ruler = false

-- Keep cursor at the start of line
vim.opt.startofline = true

-- Save undo history
vim.opt.undofile = true

-- Disable auto eol insert
vim.opt.fixeol = false

-- Case-insensitive searching UNLESS \C or capital in search
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Show signcolumn by default
vim.opt.signcolumn = "yes"

-- Disable line wrapping
vim.opt.wrap = false

vim.opt.list = false
vim.opt.listchars:append({ tab = "» ", eol = "↴" })
vim.opt.listchars:append({ extends = "…", precedes = "…" })
vim.opt.listchars:append({ space = "·", trail = "·", nbsp = "·" })
vim.opt.fillchars:append({ diff = "╱", fold = "-", eob = " " })

vim.opt.foldenable = false
vim.opt.foldtext = "folded"
vim.opt.foldcolumn = "0"

-- Hide foldcolumn in diff view
vim.opt.diffopt:append("foldcolumn:0")

-- Decrease update time
vim.opt.updatetime = 300
vim.opt.timeoutlen = 300
vim.opt.timeout = false

-- Command-line completion mode
vim.opt.wildmode = "longest:full,full"

-- Configure how new splits should be opened
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.splitkeep = "topline"

-- Preview substitutions live, as you type!
vim.opt.inccommand = "split"

-- Show which line your cursor is on
vim.opt.cursorline = true
vim.opt.cursorlineopt = "number"

-- Minimal number of screen lines to keep above and below the cursor.
vim.opt.scrolloff = 1
vim.opt.sidescrolloff = 3

-- Default tab size options
vim.opt.expandtab = false
vim.opt.tabstop = 4
vim.opt.shiftwidth = 0

-- Disable swapfile
vim.opt.swapfile = false

-- Do not show intro message when starting Vim
vim.opt.shortmess:append({ I = true })
