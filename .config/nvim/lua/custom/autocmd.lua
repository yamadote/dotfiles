-- See `:help autocmd`
local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

autocmd("VimResized", {
	group = augroup("auto_resize", { clear = true }),
	command = "wincmd =",
})

autocmd("TextYankPost", {
	group = augroup("kickstart_highlight_yank", { clear = true }),
	callback = function()
		vim.highlight.on_yank()
	end,
})

autocmd({ "InsertEnter", "BufEnter" }, {
	group = augroup("clear_search_highlight", { clear = true }),
	callback = function()
		vim.schedule(function()
			vim.cmd("nohlsearch")
		end)
	end,
})

autocmd({ "BufWritePre" }, {
	desc = "Create directory when saving a file",
	group = augroup("write_dir_check", { clear = true }),
	callback = function(event)
		if event.match:match("^%w%w+://") then
			return
		end
		local file = vim.uv.fs_realpath(event.match) or event.match
		vim.fn.mkdir(vim.fn.fnamemodify(file, ":p:h"), "p")
	end,
})

autocmd({ "TermOpen" }, {
	group = augroup("terminal_setup", { clear = true }),
	callback = function()
		vim.opt_local.number = false
		vim.opt_local.signcolumn = "auto"
		vim.cmd("startinsert")
	end,
})

autocmd({ "VimEnter", "WinEnter", "BufEnter", "InsertLeave" }, {
	group = augroup("cursorline_enable", { clear = true }),
	callback = function()
		vim.opt_local.cursorline = true
	end,
})

autocmd({ "InsertEnter", "WinLeave", "BufLeave" }, {
	group = augroup("cursorline_disable", { clear = true }),
	callback = function()
		vim.opt_local.cursorline = false
	end,
})

autocmd("BufReadPost", {
	group = augroup("buffer_initialize", { clear = true }),
	callback = function(event)
		autocmd("BufWinEnter", {
			once = true,
			buffer = event.buf,
			callback = function()
				-- go to the last position
				local patterns = { "git", "gitcommit", "gitrebase", "fugitive" }
				if not vim.tbl_contains(patterns, vim.bo[event.buf].filetype) then
					vim.cmd([[silent! normal! g`"]])
				end

				-- move fugitive window to top
				if vim.bo[event.buf].filetype == "fugitive" then
					vim.cmd("wincmd K")
				end
			end,
		})
	end,
})

autocmd({ "WinLeave" }, {
	group = augroup("autoclose_windows", { clear = true }),
	callback = function(event)
		local patterns = { "aerial", "lazy" }
		if vim.tbl_contains(patterns, vim.bo[event.buf].filetype) then
			vim.cmd("close")
		end
	end,
})

autocmd("FileType", {
	group = augroup("close_with_q", { clear = true }),
	pattern = { "vim", "help", "man", "git", "fugitive", "fugitiveblame", "qf" },
	callback = function(event)
		vim.bo[event.buf].buflisted = false
		vim.schedule(function()
			vim.keymap.set("n", "q", "<C-w>q", { buffer = event.buf })
		end)
	end,
})

autocmd("FileType", {
	group = augroup("static_window_options", { clear = true }),
	pattern = { "vim", "help", "man", "git", "fugitive", "fugitiveblame", "checkhealth", "tutor" },
	callback = function()
		vim.opt_local.signcolumn = "auto"
		vim.opt_local.foldcolumn = "0"
		vim.opt_local.list = false
		vim.opt_local.number = false
	end,
})

autocmd("LspAttach", {
	group = augroup("custom_lsp_attach", { clear = true }),
	callback = function(event)
		local map = function(mode, keys, func)
			vim.keymap.set(mode, keys, func, { buffer = event.buf })
		end

		-- Jump to the definition of the word under your cursor.
		map("n", "<CR>", require("telescope.builtin").lsp_definitions)

		-- Find references for the word under your cursor.
		map("n", "gr", require("telescope.builtin").lsp_references)

		-- Jump to the implementation of the word under your cursor.
		map("n", "gi", require("telescope.builtin").lsp_implementations)

		-- Rename the variable under your cursor
		map("n", "<leader>rn", vim.lsp.buf.rename)

		-- Execute a code action
		map("n", "<leader>ca", vim.lsp.buf.code_action)

		-- Opens a popup that displays documentation about the word under your cursor
		map("n", "K", vim.lsp.buf.hover)

		-- Add border for documentation window
		vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = vim.g.border })

		-- Jump to the declaration of the word under the cursor.
		map("n", "gd", vim.lsp.buf.declaration)

		local client = vim.lsp.get_client_by_id(event.data.client_id)
		if client then
			-- Disable lsp semantic highlights
			client.server_capabilities.semanticTokensProvider = nil
		end
	end,
})

autocmd("DirChanged", {
	group = augroup("directory_change", { clear = true }),
	callback = function()
		vim.fn.execute("!tmux refresh-client -S", "silent")
	end,
})
