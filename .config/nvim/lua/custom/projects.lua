local M = {}

function M.setup(projects)
	local function fetch_projects()
		local results = {}
		local list = projects:get({ order_by = { desc = "rowid" } })
		for _, item in ipairs(list) do
			table.insert(results, item.path)
		end
		return results
	end

	local function new_picker(opts, results)
		local function change_working_directory(prompt_bufnr)
			local state = require("telescope.actions.state")
			local selection = state.get_selected_entry()
			if selection and selection.value then
				require("telescope.actions").close(prompt_bufnr)
				vim.fn.chdir(selection.value)
				projects:remove({ path = selection.value })
				projects:insert({ path = selection.value })
			end
		end

		local function remove_entry(prompt_bufnr)
			local action_state = require("telescope.actions.state")
			local picker = action_state.get_current_picker(prompt_bufnr)
			picker:delete_selection(function(selection)
				projects:remove({ path = selection.value })
			end)
		end

		return require("telescope.pickers").new(opts, {
			prompt_title = "Search Projects",
			finder = require("telescope.finders").new_table({
				results = results,
				entry_maker = require("telescope.make_entry").gen_from_string(opts),
			}),
			sorter = require('telescope.sorters').get_substr_matcher(opts),
			attach_mappings = function(_, map)
				require("telescope.actions").select_default:replace(change_working_directory)
				map({ "i", "n" }, "<C-x>", remove_entry)
				return true
			end,
		})
	end

	-- Projects picker
	vim.keymap.set("n", "<leader>sp", function()
		local opts = { layout_strategy = "center" }
		local results = fetch_projects()
		new_picker(opts, results):find()
	end)

	-- Add project entry
	vim.keymap.set("n", "<leader>pp", function()
		local path = vim.uv.cwd()
		projects:remove({ path = path })
		projects:insert({ path = path })
		print("The project path is added")
	end)
end

return M
