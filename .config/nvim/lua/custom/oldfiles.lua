local M = {}

function M.setup(oldfiles)
	local function fetch_oldfiles()
		local current_buffer = vim.api.nvim_get_current_buf()
		local current_file = vim.api.nvim_buf_get_name(current_buffer)
		local results = {}

		local list = oldfiles:get({ order_by = { desc = "rowid" }, limit = 2000 })
		for _, item in ipairs(list) do
			local file_stat = vim.uv.fs_stat(item.file)
			local is_file = file_stat and file_stat.type == "file"
			if is_file and item.file ~= current_file then
				table.insert(results, item.file)
			end
		end
		return results
	end

	local function new_picker(opts, results)
		local function remove_entry(prompt_bufnr)
			local action_state = require("telescope.actions.state")
			local picker = action_state.get_current_picker(prompt_bufnr)
			picker:delete_selection(function(selection)
				oldfiles:remove({ file = selection.value })
			end)
		end

		return require("telescope.pickers").new(opts, {
			finder = require("telescope.finders").new_table({
				results = results,
				entry_maker = require("telescope.make_entry").gen_from_file(opts),
			}),
			sorter = require('telescope.sorters').get_substr_matcher(opts),
			previewer = require("telescope.config").values.file_previewer(opts),
			attach_mappings = function(_, map)
				map({ "i", "n" }, "<C-x>", remove_entry)
				return true
			end,
		})
	end

	-- Recent files picker
	vim.keymap.set("n", "<C-e>", function()
		local opts = { prompt_title = "Recent Files" }
		local results = fetch_oldfiles()
		new_picker(opts, results):find()
	end)

	-- Recent project files picker
	vim.keymap.set("n", "<leader>pr", function()
		local opts = { prompt_title = "Project Recent Files" }
		local results = fetch_oldfiles()

		local cwd = vim.uv.cwd()
		results = vim.tbl_filter(function(file)
			return file:sub(1, #cwd) == cwd
		end, results)

		new_picker(opts, results):find()
	end)

	-- Save oldfiles entry
	vim.api.nvim_create_autocmd({ "BufEnter" }, {
		group = vim.api.nvim_create_augroup("save_oldfiles_entry", { clear = true }),
		callback = function()
			local path = vim.fn.expand("%:p")
			if path == vim.g.last_oldfile_inserted or vim.wo.previewwindow then
				return
			end
			if path:match("^/tmp/") or path:match("/COMMIT_EDITMSG$") then
				return
			end
			if not path:match("^/") then
				return
			end
			oldfiles:remove({ file = path })
			oldfiles:insert({ file = path })
			vim.g.last_oldfile_inserted = path
		end,
	})
end

return M
