local actions = require("telescope.actions")
local finders = require("telescope.finders")
local make_entry = require("telescope.make_entry")
local pickers = require("telescope.pickers")
local previewers = require("telescope.previewers")
local utils = require("telescope.utils")
local conf = require("telescope.config").values
local sorters = require("telescope.sorters")
local job = require("plenary.job")

-- helper function for diff buffer previewer
local function diff_preview_maker(cmd, bufnr, opts)
	local function on_exit(j)
		if vim.api.nvim_buf_is_valid(bufnr) then
			local result = {}
			for i, v in ipairs(j:result()) do
				-- skips diff header
				if i > 4 then
					table.insert(result, v)
				end
			end
			vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, result)

			local putils = require("telescope.previewers.utils")
			putils.regex_highlighter(bufnr, "diff")
		end
	end

	local command = table.remove(cmd, 1)
	job:new({
		command = command,
		args = cmd,
		env = opts.env,
		cwd = opts.cwd,
		on_exit = vim.schedule_wrap(on_exit),
	}):start()
end

local M = {}

function M.git_branches()
	local opts = { layout_strategy = "center" }
	local cmd = { "git", "branch", "--format", "%(refname:short)" }
	local results = utils.get_os_command_output(cmd, opts.cwd)

	if #results == 0 then
		return
	end

	local picker = pickers.new(opts, {
		prompt_title = "Git Branches",
		finder = finders.new_table({
			results = results,
			entry_maker = make_entry.gen_from_string(opts),
		}),
		sorter = sorters.get_substr_matcher(opts),
		attach_mappings = function()
			actions.select_default:replace(actions.git_checkout)
			return true
		end,
	})
	picker:find()
end

function M.git_diff()
	local opts = {}
	local git_cmd = { "git", "diff", "--name-only", "--relative" }
	local results = utils.get_os_command_output(git_cmd, opts.cwd)

	if #results == 0 then
		vim.notify("[telescope.git_diff]: No changed files found", vim.log.levels.WARN)
		return
	end

	local previewer = previewers.new_buffer_previewer({
		title = "Git File Diff Preview",
		get_buffer_by_name = function(_, entry)
			return entry.value
		end,
		define_preview = function(self, entry, status)
			diff_preview_maker({ "git", "--no-pager", "diff", "--", entry.value }, self.state.bufnr, {
				value = entry.value,
				bufname = self.state.bufname,
				cwd = opts.cwd,
			})
		end,
	})

	local picker = pickers.new(opts, {
		prompt_title = "Git Diff",
		finder = finders.new_table({
			results = results,
			entry_maker = make_entry.gen_from_file(opts),
		}),
		previewer = previewer,
		sorter = sorters.get_substr_matcher(opts),
	})
	picker:find()
end

function M.git_staged()
	local opts = {}
	local git_cmd = { "git", "diff", "--name-only", "--cached", "--relative" }
	local results = utils.get_os_command_output(git_cmd, opts.cwd)

	if #results == 0 then
		vim.notify("[telescope.git_staged]: No staged files found", vim.log.levels.WARN)
		return
	end

	local previewer = previewers.new_buffer_previewer({
		title = "Git File Diff Preview",
		get_buffer_by_name = function(_, entry)
			return entry.value
		end,
		define_preview = function(self, entry, status)
			diff_preview_maker({ "git", "--no-pager", "diff", "--cached", "--", entry.value }, self.state.bufnr, {
				value = entry.value,
				bufname = self.state.bufname,
				cwd = opts.cwd,
			})
		end,
	})

	local picker = pickers.new(opts, {
		prompt_title = "Git Staged",
		finder = finders.new_table({
			results = results,
			entry_maker = make_entry.gen_from_file(opts),
		}),
		previewer = previewer,
		sorter = sorters.get_substr_matcher(opts),
	})
	picker:find()
end

function M.git_untracked()
	local opts = {}
	local git_cmd = { "git", "ls-files", "--others", "--exclude-standard" }
	local results = utils.get_os_command_output(git_cmd, opts.cwd)

	if #results == 0 then
		vim.notify("[telescope.git_untracked]: No untracked files found", vim.log.levels.WARN)
		return
	end

	local picker = pickers.new(opts, {
		prompt_title = "Git Untracked",
		finder = finders.new_table({
			results = results,
			entry_maker = make_entry.gen_from_file(opts),
		}),
		previewer = conf.file_previewer(opts),
		sorter = sorters.get_substr_matcher(opts),
	})
	picker:find()
end

function M.directories()
	local opts = {}
	local git_cmd = { "fd", "--type", "directory" }
	local results = utils.get_os_command_output(git_cmd, opts.cwd)

	local picker = pickers.new(opts, {
		prompt_title = "Project Directories",
		finder = finders.new_table({
			results = results,
			entry_maker = make_entry.gen_from_file(opts),
		}),
		previewer = conf.file_previewer(opts),
		sorter = conf.file_sorter(opts),
	})
	picker:find()
end

return M
