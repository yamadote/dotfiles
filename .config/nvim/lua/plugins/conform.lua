return {
	"stevearc/conform.nvim",
	event = "VeryLazy",
	config = function()
		require("conform").setup({
			formatters_by_ft = {
				lua = { "stylua" },
				go = { "goimports", "gofmt" },
				php = { "php_cs_fixer" },
				javascript = { "prettier" },
				typescript = { "prettier" },
				css = { "prettier" },
				scss = { "prettier" },
				html = { "prettier" },
				markdown = { "prettier" },
				["_"] = { "trim_whitespace" },
			},
		})
		vim.keymap.set("n", "<leader>f", function()
			require("conform").format()
		end)
	end,
}
