return {
	"nvim-telescope/telescope.nvim",
	event = "VeryLazy",
	branch = "0.1.x",
	dependencies = { "nvim-lua/plenary.nvim" },
	config = function()
		-- [[ Configure Telescope ]]
		-- See `:help telescope` and `:help telescope.setup()`
		local actions = require("telescope.actions")
		local toggle_preview = require("telescope.actions.layout").toggle_preview
		local builtin = require("telescope.builtin")
		local custom = require("custom.pickers")

		local layout_strategies = require("telescope.pickers.layout_strategies")
		layout_strategies.vertical_merged = function(picker, max_columns, max_lines, layout_config)
			local layout = layout_strategies.vertical(picker, max_columns, max_lines, layout_config)
			layout.results.line = layout.results.line - 1
			layout.results.height = layout.results.height + 1
			if layout.preview then
				layout.preview.line = layout.preview.line - 1
				layout.preview.height = layout.preview.height + 2
				layout.preview.border = { 0, 1, 0, 1 }
			end
			return layout
		end

		local function yank_entry(prompt_bufnr)
			local state = require("telescope.actions.state")
			local selection = state.get_selected_entry()
			if selection and selection.value then
				require("telescope.actions").close(prompt_bufnr)
				vim.schedule(function()
					vim.fn.setreg("+", selection.value)
					print('The entry yanked into "+')
				end)
			end
		end

		local defaults = {
			prompt_prefix = " > ",
			selection_caret = " ",
			entry_prefix = " ",
			layout_strategy = "vertical_merged",
			sorting_strategy = "ascending",
			path_display = { "truncate" },
			scroll_strategy = "limit",
			preview = { hide_on_startup = true },
			results_title = false,
		}

		defaults.layout_config = {
			vertical = {
				preview_cutoff = 0,
				prompt_position = "top",
				preview_height = 0.6,
				mirror = true,
			},
		}

		defaults.borderchars = {
			prompt = { "─", "│", "─", "│", "┌", "┐", "│", "│" },
			results = { "─", "│", "─", "│", "│", "│", "┘", "└" },
			preview = { " ", " ", " ", " ", " ", " ", " ", " " },
		}

		defaults.mappings = {
			i = {
				["<esc>"] = actions.close,
				["<C-space>"] = toggle_preview,
				["<C-f>"] = actions.to_fuzzy_refine,
				["<C-y>"] = yank_entry,
			},
		}

		local pickers = {}

		pickers.find_files = {
			prompt_title = "Project Files",
			hidden = true,
		}

		pickers.live_grep = {
			prompt_title = "Project Live Grep",
			mappings = { i = { ["<C-space>"] = toggle_preview } },
		}

		pickers.buffers = {
			mappings = { i = { ["<C-x>"] = "delete_buffer" } },
			sort_mru = true,
		}

		pickers.lsp_dynamic_workspace_symbols = {
			mappings = { i = { ["<C-space>"] = toggle_preview } },
		}

		pickers.lsp_references = {
			preview = { hide_on_startup = false },
			include_declaration = false,
			show_line = false,
		}

		pickers.lsp_implementations = {
			preview = { hide_on_startup = false },
			show_line = false,
		}

		pickers.lsp_definitions = {
			preview = { hide_on_startup = false },
			show_line = false,
		}

		pickers.lsp_document_symbols = {
			preview = { hide_on_startup = false },
			symbol_highlights = {},
			symbol_width = 80,
		}

		pickers.help_tags = {
			prompt_title = "Help Tags",
		}

		pickers.man_pages = {
			prompt_title = "Man Pages",
		}

		pickers.git_stash = {
			preview = { hide_on_startup = false },
			show_branch = false,
		}

		require("telescope").setup({
			defaults = defaults,
			pickers = pickers,
		})

		-- Keymaps
		local map = vim.keymap.set
		map("n", "<leader>pf", builtin.find_files, { desc = "Project files" })
		map("n", "<leader>pg", builtin.live_grep, { desc = "Project grep" })
		map("n", "<leader>pw", builtin.grep_string, { desc = "Project word grep" })
		map("n", "<leader>pd", custom.directories, { desc = "Project directories" })

		map("n", "<leader>rs", builtin.resume, { desc = "Resume telescope" })
		map("n", "<leader>sm", builtin.man_pages, { desc = "Search man pages" })
		map("n", "<leader>sh", builtin.help_tags, { desc = "Search help" })
		map("n", "<leader>b", builtin.buffers, { desc = "Buffers" })

		map("n", "<leader>jf", builtin.git_files, { desc = "Git files" })
		map("n", "<leader>jd", custom.git_diff, { desc = "Git diff" })
		map("n", "<leader>js", custom.git_staged, { desc = "Git staged" })
		map("n", "<leader>jt", custom.git_untracked, { desc = "Git untracked" })
		map("n", "<leader>jb", custom.git_branches, { desc = "Git branches" })
		map("n", "<leader>j@", builtin.git_stash, { desc = "Git stash" })

		-- Shortcut for searching the neovim config files
		map("n", "<leader>sc", function()
			builtin.find_files({
				cwd = vim.fn.stdpath("config"),
				prompt_title = "Search Config",
			})
		end, { desc = "Search config" })

		-- Shortcut for searching lazy files
		map("n", "<leader>sl", function()
			builtin.find_files({
				cwd = vim.fn.stdpath("data") .. "/lazy",
				prompt_title = "Search Lazy",
			})
		end, { desc = "Search lazy" })

		-- Search dotfiles
		map("n", "<leader>s.", function()
			builtin.find_files({
				cwd = os.getenv("HOME"),
				find_command = { "git", "--git-dir=" .. os.getenv("HOME") .. "/.dotfiles/", "ls-files" },
				prompt_title = "Search Dotfiles",
			})
		end, { desc = "Search dotfiles" })
	end,
}
