return {
	"catgoose/nvim-colorizer.lua",
	keys = { { "<leader>ch", "<cmd>ColorizerToggle<cr>" } },
	opts = {
		user_default_options = { names = false },
		filetypes = {},
	},
}
