return {
	"tpope/vim-fugitive",
	config = function()
		local map = vim.keymap.set
		map("n", "<leader>g", ":Git ", { desc = "Git command" })
		map("n", "<leader>jj", "<cmd>Git<cr>", { desc = "Git fugitive" })
		map("n", "<leader>je", "<cmd>Gedit HEAD:%<cr>", { desc = "Git edit" })
		map("n", "<leader>jw", "<cmd>Gwrite<cr>", { desc = "Git write" })
		map("n", "<leader>jr", "<cmd>Git restore --staged %<cr>", { desc = "Git unstage" })
		map("n", "<leader>jx", "<cmd>Git restore %<cr>", { desc = "Git restore" })

		map("n", "<leader>ja", function()
			vim.ui.input({ prompt = "Commit Message: " }, function(msg)
				if msg then
					vim.cmd('Git commit -m "' .. msg .. '"')
				end
			end)
		end, { desc = "Git commit" })

		vim.api.nvim_create_autocmd({ "TabEnter", "BufWritePost" }, {
			callback = function()
				vim.call("fugitive#ReloadStatus")
			end,
		})
	end,
}
