return {
	"stevearc/aerial.nvim",
	event = "VeryLazy",
	keys = { { "<leader>a", "<cmd>AerialOpen<cr>" } },
	opts = {
		layout = {
			min_width = 0.3,
			default_direction = "left",
			win_opts = {
				signcolumn = "yes",
				winfixbuf = true,
			},
		},

		show_guides = true,
		guides = {
			mid_item = "",
			nested_top = "",
			last_item = "",
			whitespace = "",
		},

		post_jump_cmd = false,
		highlight_on_jump = false,

		on_attach = function(bufnr)
			vim.keymap.set("n", "[a", "<cmd>AerialPrev<cr>", { buffer = bufnr })
			vim.keymap.set("n", "]a", "<cmd>AerialNext<cr>", { buffer = bufnr })
		end,

		keymaps = {
			["K"] = "actions.prev",
			["J"] = "actions.next",

			["h"] = false,
			["l"] = false,

			["H"] = false,
			["L"] = false,

			["o"] = false,
			["O"] = false,
		},
	},
}
