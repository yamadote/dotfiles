return {
	"lewis6991/gitsigns.nvim",
	opts = {
		attach_to_untracked = true,
		signs_staged_enable = false,
		preview_config = { border = vim.g.border },
		signs = {
			add = { text = "+" },
			change = { text = "~" },
			delete = { text = "⨯" },
			topdelete = { text = "⨯" },
			changedelete = { text = "~" },
			untracked = { text = " " },
		},
		worktrees = {
			{
				toplevel = vim.env.HOME,
				gitdir = vim.env.HOME .. "/.dotfiles",
			},
		},
		on_attach = function(bufnr)
			local gitsigns = require("gitsigns")

			local function map(mode, l, r, opts)
				opts = opts or {}
				opts.buffer = bufnr
				vim.keymap.set(mode, l, r, opts)
			end

			-- Navigation
			local nav_map = function(keys, direction, opts, desc)
				map("n", keys, function()
					if vim.wo.diff then
						vim.cmd.normal({ keys, bang = true })
					else
						gitsigns.nav_hunk(direction, opts)
					end
				end, { desc = desc })
			end

			nav_map("]c", "next", { target = "unstaged" }, "Next git hunk")
			nav_map("[c", "prev", { target = "unstaged" }, "Previous git hunk")

			-- Actions
			map("n", "<leader>hh", gitsigns.stage_hunk, { desc = "Hunk stage" })
			map("n", "<leader>hr", gitsigns.reset_hunk, { desc = "Hunk reset" })
			map("n", "<leader>hd", gitsigns.preview_hunk, { desc = "Hunk diff" })
			map("n", "<leader>hb", gitsigns.blame_line, { desc = "Hunk blame" })

			vim.api.nvim_create_autocmd("User", {
				pattern = "GitSignsChanged",
				callback = function()
					vim.call("fugitive#ReloadStatus")
				end,
			})
		end,
	},
}
