return {
	"sindrets/diffview.nvim",
	event = "VeryLazy",
	keys = { { "<leader>do", ":DiffviewOpen " } },
	config = function()
		local actions = require("diffview.actions")
		local prev_entry_key = { "n", "<Up>", actions.prev_entry, { desc = "Go to the previous file entry" } }
		local next_entry_key = { "n", "<Down>", actions.next_entry, { desc = "Go to the next file entry" } }

		local select_entry_key = { "n", "<CR>", actions.select_entry, { desc = "Open the selected file diff" } }
		local select_next_entry_key = { "n", "J", actions.select_next_entry, { desc = "Open the next file diff" } }
		local select_prev_entry_key = { "n", "K", actions.select_prev_entry, { desc = "Open the previous file diff" } }

		local function diff_buf_win_enter(bufnr)
			vim.opt_local.foldcolumn = "0"
			vim.opt_local.signcolumn = "yes"
		end

		local function goto_file_tab()
			actions.goto_file_tab()
		end

		local function goto_file_tabonly()
			actions.goto_file_tab()
			vim.cmd("tabonly")
		end

		local goto_file_key = { "n", "o", goto_file_tabonly, { desc = "Open the file in the previous tabpage" } }
		local goto_file_tab_key = { "n", "O", goto_file_tab, { desc = "Open the file in a new tabpage" } }
		local toggle_files_key = { "n", "<C-Space>", actions.toggle_files, { desc = "Toggle the file panel." } }

		local toggle_stage_entry_key = { "n", "<Tab>", actions.toggle_stage_entry, { desc = "Toggle the stage entry" } }
		local unstage_all_key = { "n", "U", actions.unstage_all, { desc = "Unstage all entries" } }
		local restore_entry_key = { "n", "X", actions.restore_entry, { desc = "Restore the entry" } }
		local open_commit_log_key = { "n", "C", actions.open_commit_log, { desc = "Open the commit log panel" } }

		local function help(groups)
			return { "n", "g?", actions.help(groups), { desc = "Open the help panel" } }
		end

		local keymaps = {
			disable_defaults = true,
			option_panel = {
				{ "n", "<Tab>", actions.select_entry, { desc = "Change the current option" } },
				{ "n", "q", actions.close, { desc = "Close the panel" } },
				help("option_panel"),
			},
			help_panel = {
				{ "n", "q", actions.close, { desc = "Close help menu" } },
			},
			diff1 = { help({ "view", "diff1" }) },
			diff2 = { help({ "view", "diff2" }) },
			diff3 = { help({ "view", "diff3" }) },
			diff4 = { help({ "view", "diff4" }) },
		}

		keymaps.view = {
			select_next_entry_key,
			select_prev_entry_key,

			goto_file_key,
			goto_file_tab_key,
			toggle_files_key,

			{ "n", "[x", actions.prev_conflict, { desc = "In the merge-tool: jump to the previous conflict" } },
			{ "n", "]x", actions.next_conflict, { desc = "In the merge-tool: jump to the next conflict" } },
			{ "n", "dx", actions.conflict_choose("none"), { desc = "Delete the conflict region" } },
			{ "n", "dX", actions.conflict_choose_all("none"), { desc = "Delete all conflict regions" } },
		}

		keymaps.file_panel = {
			next_entry_key,
			prev_entry_key,

			select_entry_key,
			select_next_entry_key,
			select_prev_entry_key,

			goto_file_key,
			goto_file_tab_key,
			toggle_files_key,

			{ "n", "[x", actions.prev_conflict, { desc = "Go to the previous conflict" } },
			{ "n", "]x", actions.next_conflict, { desc = "Go to the next conflict" } },
			{ "n", "dx", actions.conflict_choose("none"), { desc = "Delete the conflict region" } },
			{ "n", "dX", actions.conflict_choose_all("none"), { desc = "Delete all conflict regions" } },

			{ "n", "<BS>", actions.close_fold, { desc = "Collapse fold" } },
			{ "n", "zo", actions.open_fold, { desc = "Expand fold" } },
			{ "n", "za", actions.toggle_fold, { desc = "Toggle fold" } },
			{ "n", "zR", actions.open_all_folds, { desc = "Expand all folds" } },
			{ "n", "zM", actions.close_all_folds, { desc = "Collapse all folds" } },

			toggle_stage_entry_key,
			unstage_all_key,
			restore_entry_key,
			open_commit_log_key,

			{ "n", "i", actions.listing_style, { desc = "Toggle between 'list' and 'tree' views" } },
			{ "n", "f", actions.toggle_flatten_dirs, { desc = "Flatten empty subdirectories" } },
			{ "n", "R", actions.refresh_files, { desc = "Update stats and entries in the file list" } },

			{ "n", "<C-b>", actions.scroll_view(-0.25), { desc = "Scroll the view up" } },
			{ "n", "<C-f>", actions.scroll_view(0.25), { desc = "Scroll the view down" } },
			help("file_panel"),
		}

		keymaps.file_history_panel = {
			next_entry_key,
			prev_entry_key,

			select_entry_key,
			select_next_entry_key,
			select_prev_entry_key,

			goto_file_key,
			goto_file_tab_key,
			toggle_files_key,

			{ "n", "<BS>", actions.close_fold, { desc = "Collapse fold" } },
			{ "n", "zo", actions.open_fold, { desc = "Expand fold" } },
			{ "n", "za", actions.toggle_fold, { desc = "Toggle fold" } },
			{ "n", "zR", actions.open_all_folds, { desc = "Expand all folds" } },
			{ "n", "zM", actions.close_all_folds, { desc = "Collapse all folds" } },

			{ "n", "g!", actions.options, { desc = "Open the option panel" } },
			{ "n", "y", actions.copy_hash, { desc = "Copy the commit hash of the entry under the cursor" } },

			restore_entry_key,
			open_commit_log_key,

			{ "n", "<C-b>", actions.scroll_view(-0.25), { desc = "Scroll the view up" } },
			{ "n", "<C-f>", actions.scroll_view(0.25), { desc = "Scroll the view down" } },
			help("file_history_panel"),
		}

		local function win_config()
			return {
				position = "bottom",
				win_opts = { winfixheight = true },
				height = math.floor(vim.o.lines * 0.3),
			}
		end

		require("diffview").setup({
			use_icons = false,
			show_help_hints = false,
			enhanced_diff_hl = false,
			signs = {
				fold_closed = "* ",
				fold_open = "* ",
				done = vim.g.done_icon,
			},
			hooks = {
				diff_buf_win_enter = diff_buf_win_enter,
			},
			file_panel = {
				listing_style = "list",
				win_config = win_config,
			},
			file_history_panel = {
				win_config = win_config,
			},
			keymaps = keymaps,
		})

		vim.api.nvim_create_autocmd("FileType", {
			group = vim.api.nvim_create_augroup("diffview_open_keymap", { clear = true }),
			pattern = { "git" },
			callback = function(event)
				vim.keymap.set("n", "dd", function()
					local value = vim.fn.expand("%:t")
					vim.cmd("DiffviewOpen " .. value .. "^!")
				end, { buffer = event.buf })

				vim.keymap.set("v", "d", function()
					local range = require("custom.utils").get_visual_range()
					local first_line = require("custom.utils").get_line(0, range[1])
					local last_line = require("custom.utils").get_line(0, range[2])

					local first_hash = first_line:match("%w+")
					local last_hash = last_line:match("%w+")
					vim.cmd("DiffviewOpen " .. last_hash .. "^.." .. first_hash)
				end, { buffer = event.buf })
			end,
		})

		vim.api.nvim_create_autocmd("FileType", {
			group = vim.api.nvim_create_augroup("diffview_open_word_keymap", { clear = true }),
			pattern = { "git", "fugitive", "fugitiveblame" },
			callback = function(event)
				vim.keymap.set("n", "dw", function()
					local value = vim.fn.expand("<cword>")
					vim.cmd("DiffviewOpen " .. value .. "^!")
				end, { buffer = event.buf })
			end,
		})
	end,
}
