return {
	"romankr-dv/onedark.nvim",
	priority = 1000,
	lazy = false,
	config = function()
		require("onedark").setup()
		require("onedark").load()

		-- Disable mathparen highlight
		vim.cmd([[hi MatchParen guibg=None]])

		-- Change search highlighting
		vim.opt.winhighlight = "CurSearch:DiffText,IncSearch:DiffText,Search:DiffChange"
	end,
}
