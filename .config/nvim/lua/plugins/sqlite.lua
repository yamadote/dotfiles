return {
	"kkharji/sqlite.lua",
	config = function()
		local sqlite = require("sqlite")

		local oldfiles_tbl = sqlite.tbl("oldfiles", {
			file = { type = "text", required = true, primary = true },
		})

		local projects_tbl = sqlite.tbl("projects", {
			path = { type = "text", required = true, primary = true },
		})

		sqlite({
			uri = "/home/romankr/.local/nvim.db",
			oldfiles = oldfiles_tbl,
			projects = projects_tbl,
		})

		require("custom.oldfiles").setup(oldfiles_tbl)
		require("custom.projects").setup(projects_tbl)
	end,
}
