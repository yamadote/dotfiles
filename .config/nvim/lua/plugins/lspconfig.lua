return {
	"neovim/nvim-lspconfig",
	dependencies = {
		"folke/lazydev.nvim",
		"hrsh7th/nvim-cmp",
		"hrsh7th/cmp-nvim-lsp",

		{ "L3MON4D3/LuaSnip", build = "make install_jsregexp" },
		"saadparwaiz1/cmp_luasnip",
	},
	config = function()
		vim.diagnostic.config({
			underline = false,
			virtual_text = { severity = { min = vim.diagnostic.severity.ERROR } },
			signs = false,
			float = { header = false, border = vim.g.border },
		})

		-- `lazydev` configures Lua LSP for your Neovim config, runtime and plugins
		-- used for completion, annotations and signatures of Neovim apis
		require("lazydev").setup({
			library = {
				-- Load luvit types when the `vim.uv` word is found
				{ path = "${3rd}/luv/library", words = { "vim%.uv" } },
			},
		})

		local capabilities = vim.lsp.protocol.make_client_capabilities()
		capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())

		local function on_attach(client, bufnr)
			vim.notify(string.format("[%s] LSP Attached", client.name))
		end

		-- https://luals.github.io/wiki/settings
		require("lspconfig").lua_ls.setup({ capabilities = capabilities, on_attach = on_attach })
		require("lspconfig").intelephense.setup({ capabilities = capabilities, on_attach = on_attach })
		-- require("lspconfig").phpactor.setup({ capabilities = capabilities, on_attach = on_attach })
		require("lspconfig").gopls.setup({ capabilities = capabilities, on_attach = on_attach })
		require("lspconfig").ts_ls.setup({ capabilities = capabilities, on_attach = on_attach })

		local cmp = require("cmp")
		local luasnip = require("luasnip")
		luasnip.config.setup({})

		local function lsp_entry_filter(entry)
			if entry:get_kind() == cmp.lsp.CompletionItemKind.Text then
				return false
			end
			if entry:get_kind() == cmp.lsp.CompletionItemKind.Snippet then
				return false
			end
			return true
		end

		cmp.setup({
			snippet = {
				expand = function(args)
					luasnip.lsp_expand(args.body)
				end,
			},
			formatting = {
				format = function(_, vim_item)
					vim_item.abbr = vim_item.abbr:gsub("~$", "…")
					return vim_item
				end,
			},
			completion = {
				-- autocomplete = false,
				completeopt = "menu,menuone,noinsert",
			},
			window = {
				-- completion = { scrollbar = false, scrolloff = 1 },
				documentation = cmp.config.disable,
			},

			-- See `:help ins-completion`
			mapping = cmp.mapping.preset.insert({
				["<Tab>"] = cmp.mapping.confirm({ select = true }),
				["<C-Space>"] = cmp.mapping.complete(),
				["<C-l>"] = cmp.mapping(function()
					if luasnip.expand_or_locally_jumpable() then
						luasnip.expand_or_jump()
					end
				end, { "i", "s" }),
			}),

			sources = {
				{ name = "luasnip" },
				{ name = "lazydev", group_index = 0 },
				{ name = "nvim_lsp", entry_filter = lsp_entry_filter },
			},
		})

		require("custom.snippets").setup()
	end,
}
