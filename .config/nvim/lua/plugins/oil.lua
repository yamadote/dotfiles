return {
	"stevearc/oil.nvim",
	init = function()
		vim.g.loaded_netrwPlugin = 1
		vim.g.loaded_netrw = 1
	end,
	config = function()
		vim.keymap.set("n", "<BS>", function()
			require("oil").open()
		end)

		vim.keymap.set("n", "<leader>pe", function()
			require("oil").open(vim.uv.cwd())
		end)

		local function preview_callback(err)
			if err == nil then
				local win = vim.api.nvim_get_current_win()
				local width = math.floor(vim.o.columns * 0.3)
				vim.api.nvim_win_set_width(win, width)
			end
		end

		local function toggle_preview()
			local preview_win = require("custom.utils").get_preview_win()
			if preview_win then
				vim.api.nvim_win_close(preview_win, true)
			else
				require("oil").open_preview(nil, preview_callback)
			end
		end

		local keymaps = {
			["<CR>"] = "actions.select",
			["<C-t>"] = { "actions.select", opts = { tab = true } },
			["<C-c>"] = { "actions.close", mode = "n" },
			["<C-Space>"] = {
				callback = toggle_preview,
				desc = "Toggle preview window",
				mode = "n",
			},
			["gx"] = "actions.open_external",
			["g."] = { "actions.toggle_hidden", mode = "n" },
			["`"] = { "actions.cd", mode = "n" },
			["g?"] = { "actions.show_help", mode = "n" },
		}

		require("oil").setup({
			columns = {},
			win_options = {
				signcolumn = "yes",
				foldcolumn = "0",
			},
			preview = { border = vim.g.border },
			keymaps_help = { border = vim.g.border },
			preview_win = { preview_method = "load" },
			use_default_keymaps = false,
			keymaps = keymaps,
			view_options = {
				show_hidden = true,
				is_always_hidden = function(name)
					return name == ".."
				end,
			},
			lsp_file_methods = {
				enabled = false,
			},
		})
	end,
}
