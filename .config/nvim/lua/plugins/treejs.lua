return {
	"Wansmer/treesj",
	dependencies = { "nvim-treesitter/nvim-treesitter" },
	keys = { { "<leader>cj", "<cmd>TSJToggle<cr>", desc = "Code join toggle" } },
	config = function()
		local tsj = require("treesj")
		local lang_utils = require("treesj.langs.utils")

		tsj.setup({
			use_default_keymaps = false,
			max_join_length = 150,
			langs = {
				php = {
					arguments = lang_utils.set_preset_for_args(),
					formal_parameters = lang_utils.set_preset_for_args(),
				},
			},
		})
	end,
}
