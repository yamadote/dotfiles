#!/bin/bash

# if not running interactively, don't do anything
[[ $- != *i* ]] && return

# autostart tmux in kitty terminal
if [ -z "$TMUX" ] && [ "$TERM" = "xterm-kitty" ]; then
	exec tmux && exit;
fi

# enable git autocomplete
source /usr/share/git/completion/git-completion.bash

export PATH="$HOME/go/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.local/extra:$PATH"

alias ls='ls --color=auto --group-directories-first'
alias ll='LC_COLLATE=C ls -la'

alias grep='grep --color=auto'
alias df='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias help='$HOME/.local/bin/help.sh'
alias dir='cd "./$(fd --type dir --max-depth 3 | fzf)"'

# terminal prompt
WHITE="\[\e[m\]"; PURPLE="\[\e[35m\]"; CYAN="\[\e[36m\]"
PS1="${PURPLE}[$CYAN\u$WHITE@$CYAN\h $WHITE\W$PURPLE]$WHITE\$ "

export EDITOR="nvim"
export TERM="xterm-256color"
export FZF_DEFAULT_OPTS='--reverse'
export MANPAGER='nvim +Man!'
