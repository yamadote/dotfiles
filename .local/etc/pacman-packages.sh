#!/bin/sh

echo kitty alacritty rofi keyd
# echo lightdm lightdm-gtk-greeter elementary-wallpapers
echo sddm plasma dolphin gwenview okular ark power-profiles-daemon
echo kate kcalc spectacle ktorrent kcolorchooser kolourpaint elisa
# echo xfce4 xfce4-goodies gvfs shutter xarchiver
# echo qtile picom lxappearance pavucontrol xorg-xset xorg-xrandr
# echo brightnessctl python-dbus-next network-manager-applet arandr alsa-utils

echo go git base-devel npm yarn php composer # tools
echo firefox peek vlc telegram-desktop # basic
echo ttf-jetbrains-mono ttf-noto-nerd noto-fonts noto-fonts-emoji # fonts
echo neovim xclip fd ripgrep luarocks # neovim
echo tmux htop jq curl wget less unzip cmake cloc # utils
echo man-db man-pages # man pages

echo lua-language-server stylua gopls
echo prettier typescript-language-server

# echo qutebrowser
# echo sxiv zathura lf duf dunst
# echo gnome-keyring
# echo papirus-icon-theme
# echo cargo obs-studio krita
# echo libreoffice-fresh
