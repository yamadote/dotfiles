#!/bin/sh

sudo pacman -S --needed $(.local/etc/pacman-packages.sh)

# git clone https://aur.archlinux.org/yay.git
# cd yay
# makepkg -si

# yay -S --needed ttf-ms-fonts lsyncd xkb-switch
# yay -S --needed google-chrome slack-desktop robo3t-bin postman-bin
# yay -S --needed jetbrains-toolbox tmetric-desktop
# yay -S --needed neovim-remote
# yay -S --needed phpactor php-cs-fixer

# sudo localectl set-x11-keymap "us,ua" "" "" caps:ctrl_modifier

# sudo systemctl disable lightdm.service
# sudo systemctl enable sddm
