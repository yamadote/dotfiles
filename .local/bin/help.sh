#!/bin/sh

Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White
NoColor='\033[0m'         # No Color

delimeter() {
	printf "\n"
}

item() {
	printf " %-16s   %s \n" "$1" "$2"
}

if [ "$1" = "apps" ]; then
	delimeter
	item "alacritty" "terminal"
	item "neovim" "text editor"
	item "firefox" "web browser"
	item "thunar" "file manager"
	item "ristretto" "image viewer"
	item "vlc" "music/video viewer"
	item "xfce4-appfinder" "laucher"
	item "rofi" "custom dmenu"
	delimeter
	item "pavucontrol" "sound settings"
	item "lxappearance" "theme settings"
	item "duf" "disk usage/free utility"
	item "htop" "system monitor"
	delimeter
	item "grep" "search text"
	item "sed" "replace text"
	item "fzf" "fazy file finder"
	item "cal" "calendar"
	item "date" "time provider"
	item "python3" "calculator"
	delimeter
	exit 1
fi

if [ "$1" = "pacman" ]; then
	delimeter
	item "pacman -Syu" "update system"
	item "pacman -Q" "show all installed packages"
	item "pacman -Qe" "show manually installed packages"
	item "pacman -Qql" "show installed files"
	delimeter
	exit 1
fi

if [ "$1" = "vim" ]; then
	delimeter
	item "<C-o>" "go back"
	item "<C-i>" "go forward"
	item "<C-w>q" "quit window"
	delimeter
	item "\"+y" "copy into clipboard"
	item ":cd %:h" "change current directory"
	delimeter
	item "<C-e>" "fzf recent files"
	item "<leader>sb" "search buffers"
	item "<leader>sp" "search projects"
	item "<leader>pf" "search files"
	item "<leader>pg" "search by grep"
	delimeter
	item "gd" "goto definition"
	item "gr" "goto reference"
	item "gi" "goto implementation"
	item "gf" "goto file"
	item "gD" "goto declaration"
	item "gcc" "comment code"
	delimeter
	exit 1
fi

if [ "$1" = "git" ]; then
	delimeter
	item "undo" "git reset HEAD~1"
	delimeter
	exit 1
fi

delimeter
echo " * apps"
echo " * pacman"
echo " * vim"
echo " * git"
delimeter

