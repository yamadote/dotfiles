#!/bin/sh

path=$2
title="www"

while [ -d "$path" ]; do
	category=$(ls $path | rofi -dmenu -p "$title")
	if [ "$category" = "" ]; then
		exit 1
	fi
	path="$path/$category"
done

if [ ! -f "$path" ]; then
	$1 "$category"
	exit 1
fi

qmlist=$(cat "$path")
choice=$(printf '%s\n' "$qmlist" | rofi -dmenu -p "$title")
if [ "$choice" = "" ]; then
	exit 1
fi

url=$(echo "$choice" | awk '{print $NF}')
$1 "$url"

